                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


https://www.thingiverse.com/thing:2700850
Brushless R/C Racing Hovercraft by jscaylor is licensed under the Creative Commons - Attribution license.
http://creativecommons.org/licenses/by/3.0/

# Summary

This is an r/c hovercraft that I designed to be relatively easy to print and assemble, as well as needing no glue for assembly. It only requires a few small screws and pieces of hardware available at any good hardware store, and of course the electronics will also be needed. The design is  modular to allow the repair of only certain parts of the craft should damage occur. Some basic tools will also be needed for assembly (any self respecting 3d printing guru should have the tools necessary).

The size of the upper hull (largest part) is small enough to fit on a 300mm build plate, so larger printers should have no problem printing. If you have a smaller printer you may have to cut and print in sections. Everything should be printed in the orientations shown in the photos, .3mm layer height for all works well. Use supports on all but the lower hull, skirt ring and skirt clips.

As you can see from the photos I have flown it quite a bit to test out it's capabilities and strength. I've flipped it a few times on pavement, rolled it a dozen or so and have run into many hard objects at a reasonable speed without sustaining major damage (a few scratches and dings so far), but a solid hit at full speed is likely to do a bit more damage. The skirt has been worn down after driving a dozen packs on very rough pavement, but still definitely usable. A sewn ripstop nylon skirt would be prefered and templates may be added in the future. Otherwise it performs very well and has plenty of control (even if using only two rudders), as well as tons of power. I have yet to get it up to full speed, but it definitely moves! Run time is about 5-7 minutes with the 1500mAh battery at full hover.

I still need to make the cover for the electronics, so that will be added soon. Thanks for looking, more updates to come! 

Short overview and basic assembly video. https://youtu.be/FuvmvP_xeic
Here's a short clip of it cruising around a small parking lot. https://youtu.be/SpGD2eGdmpg
 



Parts List:
- 2x 2205-2300kv Brushless motors (any brand will do, just confirm mounting hole spacing is 16x19mm)
- 8x 10mm Motor mounting screws (ones that come with the motors may be too short)
- 2x 20A Brushless ESCs (make sure only one has a BEC to eliminate the need for a reciever battery)
- 1x 9g Steering Servo
- 1x Transmitter and Reciever (requires at least 3 channels)
- 1x 1500mAh 3s Lipo
- 1x 5x4x3 Prop
- 1x 4x4x3 Prop (or a trimmed down 5x4x3 will work)
- 14x 3/8" long small diameter screws (about 3/32" dia. used for all screws) for thrust duct and lower hull
- 16x 1/4" long small screws for lift duct and skirt ring
- 10x 1/2" long small screws for rudders and front mount
- 1x short piece of 1/8" Brass or Copper tube (1/2" rudder screws should be able to pass through bore)
- 1x short piece of 1/32"/1mm music wire (spring steel) for pushrod
- 1 Heavy duty trash bag for skirt

Tools Needed:
- Small phillips or slotted screwdrivers (depending on what screws you use, I recommend button head coarse thread machine screws but they can be pricey)
- Assortment of hex wrenches or drivers
- Soldering iron
- Handheld drill or rotary tool (Dremel)
- 1/16" twist drill bit
- Nut driver or socket for prop nuts
- Xacto knife or razor blade of some sort
- Flat needle files and/or sandpaper
- Pliers


# Print Settings

Printer: Folgertech FT-5
Supports: Yes
Resolution: .3mm
Infill: 10-15%

Notes: 
Print one of each part, and you'll need about 75 of the skirt clips to cover the entire perimeter of the upper hull. You don't necessarily need to use them all to secure the skirt but they do act as kind of a bumper to protect the edge of the hull.