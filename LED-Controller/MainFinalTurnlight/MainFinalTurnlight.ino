//Modern turn lights v.4.0.0 for bikes with arduino and ws2812b - by Fedaceag Ionut
#include <FastLED.h>                        //FastLed library version 3.2.1 - https://github.com/FastLED/FastLED/wiki/Overview or http://fastled.io/ with NEOPIXEL or WS2812B
#include <PinChangeInterrupt.h>
const byte channel_pin[] = {9, 10, 11};
volatile unsigned long rising_start[] = {0, 0, 0};
volatile long channel_length[] = {0, 0, 0};
int value = 0;

#define NUM_STRIPS 4                        // number of led strips
#define NUM_LEDS_PER_STRIP 8               // number of leds per each strip

CRGB leds[NUM_STRIPS][NUM_LEDS_PER_STRIP];

uint8_t gHue = 0;                           //knight rider color
unsigned long turnColor = 0xff6a00;         //turn signal color

//input pins
const int buttonPinL = 9;                   // turn left
const int buttonPinR = 8;                   // turn right
const int buttonPinKnightRider = 5;         // knight rider lights
const int buttonPinNightLight = 4;          // night lights
const int buttonPinBrake = 6;               // brake lights
const int buttonPinHazardLights = 7;        // hazard lights

int buttonStateL = 0;
int buttonStateR = 1;
int KnightRider = 0;
int KnightRiderState = 0;
int KnightRiderPrevState = 0;
int KnightRidersChange = 0;
int KnightRidersChange2 = 0;
int nightLights = 0;
int nightLightsChange = 0;
int prevNightLights = 0;
int Brake = 0;
int BrakeChange = 0;
int hazardLights = 0;
int prevHazardLights = 0;
int hazardLightsChange = 0;
int TailOn = 0;

int stateLT = 0;
int stateRT = 0;
uint8_t gBrtN = 0;
uint8_t gBrtN2 = 0;

int maxBrt = 200;                                              // maxim brightness front night lights - from 0 to 254
int maxBrtBrake = 254;                                         // maxim brightness Brake lights - from 0 to 254
int maxBrtTailLights = 78;                                     // maxim brightness tail lights - from 0 to 254
int tailLightModel[] = {0,3,4,5,6,9};                          // model array for tail lights pattern from 0 - 9
int knightRiderSpeed = 45;                                     // speed for knight rider lights
int knightRiderTail = 10;                                      // tail effect for knight rider lights

int delayTurnLedAnim = 20;                                     //delay of each led in turn light animation
int delayTurnLedOff = 250;                                     //delay from animation to black (is used twice)
int delayLedToDayLight = 500;                                  // delay from animation to day light on
int nrAnimAfterOff = 4;                                        //number of animations for a single impulse

unsigned long currentMillis = 0; 
unsigned long previousMillis = 0;

void setup() { 
  Serial.begin(115200);
 /* pinMode(buttonPinL, INPUT);
  pinMode(buttonPinR, INPUT);
  pinMode(buttonPinKnightRider, INPUT);
  pinMode(buttonPinBrake, INPUT);
  pinMode(buttonPinNightLight, INPUT);
  pinMode(buttonPinHazardLights, INPUT);*/
      
  FastLED.addLeds<NEOPIXEL, 3>(leds[0], NUM_LEDS_PER_STRIP); //led strip front night light
  FastLED.addLeds<NEOPIXEL, 3>(leds[1], NUM_LEDS_PER_STRIP); //led strip front turn signals and night lights
  FastLED.addLeds<NEOPIXEL, 3>(leds[2], NUM_LEDS_PER_STRIP); //led strip rear night tail and brake lights
  FastLED.addLeds<NEOPIXEL, 3>(leds[3], NUM_LEDS_PER_STRIP); //led strip rear turn signals

attachPinChangeInterrupt(digitalPinToPinChangeInterrupt(channel_pin[0]), onRising0, CHANGE);
//attachPinChangeInterrupt(digitalPinToPinChangeInterrupt(channel_pin[1]), onRising1, CHANGE);
//attachPinChangeInterrupt(digitalPinToPinChangeInterrupt(channel_pin[2]), onRising2, CHANGE);

  
//  attachInterrupt(digitalPinToInterrupt(buttonPinL),btnPressL,RISING); // we use interrupt for instant reaction of turn lights
//  attachInterrupt(digitalPinToInterrupt(buttonPinR),btnPressR,RISING); // we use interrupt for instant reaction of turn lights

  fill_solid(leds[0], NUM_LEDS_PER_STRIP, CRGB::Black); // some led strips are all on at power on, so let's power them off at boot
  fill_solid(leds[1], NUM_LEDS_PER_STRIP, CRGB::Black); // some led strips are all on at power on, so let's power them off at boot
  fill_solid(leds[2], NUM_LEDS_PER_STRIP, CRGB::Black); // some led strips are all on at power on, so let's power them off at boot
  fill_solid(leds[3], NUM_LEDS_PER_STRIP, CRGB::Black); // some led strips are all on at power on, so let's power them off at boot
  FastLED.show();
}

void loop() {
  // read the input state
    /*buttonStateL = digitalRead(buttonPinL); 
    buttonStateR = digitalRead(buttonPinR);
    Serial.println(channel_length[0]);
  if (channel_length[0]>1000){
   btnPressR();
  }*/
  Serial.println(channel_length[0]);
    if (channel_length[0]>1700 ){
     // buttonStateR = HIGH;
     btnPressR();
      Serial.println("Right");
    }
    if (channel_length[0]<1100){
      //buttonStateL = HIGH;
      btnPressL();
      Serial.println("Left");
    }
    if (channel_length[0]>1200 && channel_length[0]< 1600){
      stateRT = 0;
      stateLT = 0;
    }
    /*hazardLights = digitalRead(buttonPinHazardLights);
    nightLights = digitalRead(buttonPinNightLight);
    KnightRider = digitalRead(buttonPinKnightRider);*/
    
    if(hazardLights != 0){
      hazardLightsChange = 1;
    }else if(hazardLightsChange != 0){
      if(KnightRiderState == 1){
        KnightRiderPrevState = 1;
        KnightRiderState = 0;
      }else if(KnightRiderPrevState == 1){
        KnightRiderState = 1;
        KnightRiderPrevState = 0;
      }
      prevHazardLights = !prevHazardLights;
      hazardLightsChange = 0;
    }else if(prevHazardLights != 0){    
        buttonStateL = !buttonStateL;
        stateLT = !stateLT;
        buttonStateR = !buttonStateR;
        stateRT = !stateRT;
    }

    if(nightLights != 0){
        nightLightsChange = 1;
    }else if(nightLightsChange != 0){
      prevNightLights = !prevNightLights;
      nightLightsChange = 0;
    }

    if(KnightRider != 0){
      KnightRidersChange = 1;
    }else if(KnightRidersChange != 0){
      if(prevHazardLights == 0){
        KnightRiderState = !KnightRiderState;        
      }
      KnightRidersChange = 0;      
    }
    
  //function for hazard lights
  if(stateLT != 0 && stateRT != 0){

    int hazardLeftTurn = (NUM_LEDS_PER_STRIP/2);
    int hazardRightTurn = (NUM_LEDS_PER_STRIP/2)-1;
    for(int dot = 0; dot < NUM_LEDS_PER_STRIP/2; dot++){
        leds[1][hazardLeftTurn] = turnColor;
        leds[3][hazardLeftTurn] = turnColor;
        leds[1][hazardRightTurn] = turnColor;
        leds[3][hazardRightTurn] = turnColor;
        if(prevNightLights == 0 && Brake == 0){
          leds[0][hazardLeftTurn] = turnColor;
          leds[0][hazardRightTurn] = turnColor;
          leds[2][hazardLeftTurn] = turnColor;
          leds[2][hazardRightTurn] = turnColor;
        }else if(prevNightLights == 0){
          leds[0][hazardLeftTurn] = turnColor;
          leds[0][hazardRightTurn] = turnColor;
        }
        if(BrakeChange == 1){         
          fill_solid(leds[2], NUM_LEDS_PER_STRIP, CRGB::Black);      
          BrakeChange = 0;
        }
        FastLED.show();
        
        //delay(delayTurnLedAnim);
        currentMillis = previousMillis = millis();
        while(previousMillis + delayTurnLedAnim*2 >= currentMillis){
          TailAndBrake();
          FastLED.show();
          currentMillis = millis();
        }
        
        hazardLeftTurn++;
        hazardRightTurn--;
    }
    
    //delay(delayTurnLedOff);       
    currentMillis = previousMillis = millis();
    while(previousMillis + delayTurnLedOff >= currentMillis){
      TailAndBrake();
      FastLED.show();
      currentMillis = millis();
    }
    
    fill_solid(leds[1], NUM_LEDS_PER_STRIP, CRGB::Black);
    fill_solid(leds[3], NUM_LEDS_PER_STRIP, CRGB::Black);
    if(prevNightLights == 0 && Brake == 0){
      fill_solid(leds[0], NUM_LEDS_PER_STRIP, CRGB::Black); 
      fill_solid(leds[2], NUM_LEDS_PER_STRIP, CRGB::Black);         
    }else if(prevNightLights == 0){
      fill_solid(leds[0], NUM_LEDS_PER_STRIP, CRGB::Black);          
    }
    FastLED.show();
    
    //delay(delayTurnLedOff);       
    currentMillis = previousMillis = millis();
    while(previousMillis + delayTurnLedOff >= currentMillis){
      TailAndBrake();
      FastLED.show();
      currentMillis = millis();
    }

    if(buttonStateL != HIGH || buttonStateR != HIGH){
      
      if(buttonStateL == HIGH){
        stateLT = 1;
        Serial.println("StateLT");
      }else{
        stateLT = 0;
        gBrtN = 0;
      }
      
      if(buttonStateR == HIGH){
        stateRT = 1;
                Serial.println("StateLT");

      }else{
        stateRT = 0;
        gBrtN = 0;
      }
      
      if(buttonStateL != HIGH && buttonStateR != HIGH){
        currentMillis = previousMillis = millis();
        while(previousMillis + delayLedToDayLight >= currentMillis){
          TailAndBrake();
          FastLED.show();
          currentMillis = millis();
        }
        
      }
      
    }

  //function for left turn lights
  }else if(stateLT != 0){
    
    if(KnightRiderState == HIGH && prevNightLights == LOW){
      //fill_solid(leds[1], NUM_LEDS_PER_STRIP, CRGB::Black);
    }
    
    for(int dot = 0; dot < NUM_LEDS_PER_STRIP; dot++){
        leds[1][dot] = turnColor;
        leds[3][dot] = turnColor;
        if(prevNightLights == 0 && Brake == 0){
          leds[0][dot] = turnColor;
          leds[2][dot] = turnColor;         
        }else if(prevNightLights == 0){
          leds[0][dot] = turnColor;         
        }
        if(BrakeChange == 1){         
          fill_solid(leds[2], NUM_LEDS_PER_STRIP, CRGB::Black);      
          BrakeChange = 0;
        }
        FastLED.show();
        
        //delay(delayTurnLedAnim);
        currentMillis = previousMillis = millis();
        while(previousMillis + delayTurnLedAnim >= currentMillis){
          TailAndBrake();
          FastLED.show();
          currentMillis = millis();
        }
    }
    
    //delay(delayTurnLedOff);       
    currentMillis = previousMillis = millis();
    while(previousMillis + delayTurnLedOff >= currentMillis){
      TailAndBrake();
      FastLED.show();
      currentMillis = millis();
    }
    
    fill_solid(leds[1], NUM_LEDS_PER_STRIP, CRGB::Black);
    fill_solid(leds[3], NUM_LEDS_PER_STRIP, CRGB::Black);
    if(prevNightLights == 0 && Brake == 0){
      fill_solid(leds[0], NUM_LEDS_PER_STRIP, CRGB::Black); 
      fill_solid(leds[2], NUM_LEDS_PER_STRIP, CRGB::Black);         
    }else if(prevNightLights == 0){
      fill_solid(leds[0], NUM_LEDS_PER_STRIP, CRGB::Black);          
    }
    FastLED.show();
    
    //delay(delayTurnLedOff);       
    currentMillis = previousMillis = millis();
    while(previousMillis + delayTurnLedOff >= currentMillis){
      TailAndBrake();
      FastLED.show();
      currentMillis = millis();
    }

    stateLT++;
    if(stateLT >= nrAnimAfterOff && buttonStateL != HIGH){
      stateLT = 0;
      gBrtN = 0;
      //delay(delayLedToDayLight);           
      currentMillis = previousMillis = millis();
      while(previousMillis + delayLedToDayLight >= currentMillis){
        TailAndBrake();
        FastLED.show();
        currentMillis = millis();
      }      
    }

  //function for right turn lights
  }else if(stateRT != 0){
       
    if(KnightRiderState == HIGH && prevNightLights == LOW){
      //fill_solid(leds[0], NUM_LEDS_PER_STRIP, CRGB::Black);
    }
    for(int dot = NUM_LEDS_PER_STRIP-1; dot > -1; dot--) {
      leds[1][dot] = turnColor;
      leds[3][dot] = turnColor;
      if(prevNightLights == 0 && Brake == 0){
        leds[0][dot] = turnColor;
        leds[2][dot] = turnColor;         
      }else if(prevNightLights == 0){
        leds[0][dot] = turnColor;         
      }
      if(BrakeChange == 1){         
        fill_solid(leds[2], NUM_LEDS_PER_STRIP, CRGB::Black);      
        BrakeChange = 0;
      }
      FastLED.show();

      //delay(delayTurnLedAnim);
      currentMillis = previousMillis = millis();
      while(previousMillis + delayTurnLedAnim >= currentMillis){
        TailAndBrake();
        FastLED.show();
        currentMillis = millis();
      }
    }
    
    //delay(delayTurnLedOff);       
    currentMillis = previousMillis = millis();
    while(previousMillis + delayTurnLedOff >= currentMillis){
      TailAndBrake();
      FastLED.show();
      currentMillis = millis();
    }
    
    fill_solid(leds[1], NUM_LEDS_PER_STRIP, CRGB::Black);
    fill_solid(leds[3], NUM_LEDS_PER_STRIP, CRGB::Black);
    if(prevNightLights == 0 && Brake == 0){
      fill_solid(leds[0], NUM_LEDS_PER_STRIP, CRGB::Black); 
      fill_solid(leds[2], NUM_LEDS_PER_STRIP, CRGB::Black);         
    }else if(prevNightLights == 0){
      fill_solid(leds[0], NUM_LEDS_PER_STRIP, CRGB::Black);          
    }
    FastLED.show();
    
    //delay(delayTurnLedOff);       
    currentMillis = previousMillis = millis();
    while(previousMillis + delayTurnLedOff >= currentMillis){
      TailAndBrake();
      FastLED.show();
      currentMillis = millis();
    }

    stateRT++;
    if(stateRT >= nrAnimAfterOff && buttonStateR != HIGH){
      stateRT = 0;
      gBrtN = 0;
      
      //delay(delayLedToDayLight);           
      currentMillis = previousMillis = millis();
      while(previousMillis + delayLedToDayLight >= currentMillis){
        TailAndBrake();
        FastLED.show();
        currentMillis = millis();
      }
    }
    
  }else{
    if(prevNightLights == 1){
           
      if(KnightRiderState == 1){
        
        //EVERY_N_MILLISECONDS( 20 ) { gHue++; } //uncomment this line for rainbow effect on knight rider lights
        fadeToBlackBy( leds[1], NUM_LEDS_PER_STRIP, knightRiderTail);
        fadeToBlackBy( leds[3], NUM_LEDS_PER_STRIP, knightRiderTail);
        int pos = beatsin16( knightRiderSpeed, 0, NUM_LEDS_PER_STRIP-1 );
        leds[1][pos] += CHSV( gHue, 255, 254);
        leds[3][pos] += CHSV( gHue, 255, 254);
        gBrtN = 0;
        KnightRidersChange2 = 1;
        
       }else if(KnightRidersChange2 == 1){
        
        fill_solid(leds[3], NUM_LEDS_PER_STRIP, CRGB::Black);
        KnightRidersChange2 = 0;
        
      }else if(gBrtN <= maxBrt && KnightRiderState == 0){
        
        EVERY_N_MILLISECONDS( 1 ) { gBrtN++; } 
        fill_solid( leds[1], NUM_LEDS_PER_STRIP, CHSV(0,0,gBrtN));
               
      }
      
      if(gBrtN2 <= maxBrt){
        EVERY_N_MILLISECONDS( 1 ) { gBrtN2++; } 
        fill_solid( leds[0], NUM_LEDS_PER_STRIP, CHSV(0,0,gBrtN2));
      }
      
    }else{
      
      if(gBrtN > 0){
        EVERY_N_MILLISECONDS( 1 ) { gBrtN--; } 
        fill_solid( leds[1], NUM_LEDS_PER_STRIP, CHSV(0,0,gBrtN));
      }else{
        if(KnightRiderState != 0){
          
          //EVERY_N_MILLISECONDS( 20 ) { gHue++; } //uncomment this line for rainbow effect on knight rider lights
          fadeToBlackBy( leds[1], NUM_LEDS_PER_STRIP, knightRiderTail);
          fadeToBlackBy( leds[3], NUM_LEDS_PER_STRIP, knightRiderTail);
          int pos = beatsin16( knightRiderSpeed, 0, NUM_LEDS_PER_STRIP-1 );
          leds[1][pos] += CHSV( gHue, 255, 254);
          leds[3][pos] += CHSV( gHue, 255, 254);
          
        }else{
          fill_solid(leds[1], NUM_LEDS_PER_STRIP, CRGB::Black);
        }
      }
      if(gBrtN2 > 0){
        EVERY_N_MILLISECONDS( 1 ) { gBrtN2--; } 
        fill_solid( leds[0], NUM_LEDS_PER_STRIP, CHSV(0,0,gBrtN2));
      }
    }
    
    TailAndBrake();
    FastLED.show();
  }
}

//function for Brake lights and tail lights
void TailAndBrake(){
  // read the input state
  Brake = digitalRead(buttonPinBrake);
  if(hazardLights != 0){
  hazardLightsChange = 1;
  }
  
  if(prevNightLights == 1 && Brake == 0){
    if(TailOn == 0){
      if(BrakeChange == 1){
        fill_solid(leds[2], NUM_LEDS_PER_STRIP, CRGB::Black);      
        BrakeChange = 0;
      }
      if(stateLT == 0 && stateRT == 0 && prevHazardLights == 0 && KnightRiderState == 0){
        fill_solid(leds[3], NUM_LEDS_PER_STRIP, CRGB::Black);
      }
      TailOn = 1; 
    }else if(TailOn == 1){
      for(int i = 0; i < sizeof(tailLightModel) / sizeof(tailLightModel[0]); i++){
        leds[2][tailLightModel[i]]=CHSV(0,255,maxBrtTailLights);
      }                 
      TailOn = 2;
    }
  }else if(Brake == 1){

    fill_solid( leds[2], NUM_LEDS_PER_STRIP, CHSV(0,255,maxBrtBrake));
    
    if(stateLT == 0 && stateRT == 0 && prevHazardLights == 0 && KnightRiderState == 0){
      fill_solid( leds[3], NUM_LEDS_PER_STRIP, CHSV(0,255,maxBrtBrake));
    }
    TailOn  = 0;
    BrakeChange = 1;
    
  }else{
    if(BrakeChange == 1){
      fill_solid(leds[2], NUM_LEDS_PER_STRIP, CRGB::Black);      
      BrakeChange = 0;
    }
    if(prevHazardLights == 0 && stateLT == 0 && stateRT == 0){
        fill_solid(leds[2], NUM_LEDS_PER_STRIP, CRGB::Black);      
    }
    if(stateLT == 0 && stateRT == 0 && KnightRiderState == 0){
      fill_solid(leds[3], NUM_LEDS_PER_STRIP, CRGB::Black);
    }
    TailOn = 0;
  }
  
}

//function for left signal interrupt
void btnPressL(){
  stateLT = 1;
  for(int nr = 0; nr < 15; nr++) { 
    if (channel_length[0]>1700 ){

    buttonStateR = HIGH;
    if(buttonStateR == 1){
      stateRT = 1;     
    }
    }
  }  
}

//function for right signal interrupt
void btnPressR(){
  stateRT = 1;
  for(int nr = 0; nr < 15; nr++) { 
    if (channel_length[0]<1100){
    buttonStateL = HIGH; 
    if(buttonStateL == 1){
      stateLT = 1;
    }
    }
  }
}

void processPin(byte pin) {
uint8_t trigger = getPinChangeInterruptTrigger(digitalPinToPCINT(channel_pin[pin]));
if(trigger == RISING) {
    rising_start[pin] = micros();
  } else if(trigger == FALLING) {
    channel_length[pin] = micros() - rising_start[pin];
  }
}
void onRising0(void) {
processPin(0);
}
void onRising1(void) {
processPin(1);
}
void onRising2(void) {
processPin(2);
}
