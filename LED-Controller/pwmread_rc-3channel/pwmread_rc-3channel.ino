#include <PinChangeInterrupt.h>
#include <FastLED.h>

/*
 * Define pins used to provide RC PWM signal to Arduino
 * Pins 8, 9 and 10 are used since they work on both ATMega328 and 
 * ATMega32u4 board. So this code will work on Uno/Mini/Nano/Micro/Leonardo
 * See PinChangeInterrupt documentation for usable pins on other boards
 */
 /*#if FASTLED_VERSION < 3001000
#error "Requires FastLED 3.1 or later; check github for latest code."
#endif*/

#define DATA_PIN    3
//#define CLK_PIN   4
#define LED_TYPE    WS2811
#define COLOR_ORDER GRB
#define NUM_LEDS    8
#define BRIGHTNESS  255

CRGB leds[NUM_LEDS];
const byte channel_pin[] = {2, 9, 10};
volatile unsigned long rising_start[] = {0, 0, 0};
volatile long channel_length[] = {0, 0, 0};
int value = 0;
void setup() {
  Serial.begin(115200);
pinMode(channel_pin[0], INPUT);
pinMode(channel_pin[1], INPUT);
pinMode(channel_pin[2], INPUT);
attachPinChangeInterrupt(digitalPinToPinChangeInterrupt(channel_pin[0]), onRising0, CHANGE);
attachPinChangeInterrupt(digitalPinToPinChangeInterrupt(channel_pin[1]), onRising1, CHANGE);
attachPinChangeInterrupt(digitalPinToPinChangeInterrupt(channel_pin[2]), onRising2, CHANGE);
  FastLED.addLeds<LED_TYPE,DATA_PIN,COLOR_ORDER>(leds, NUM_LEDS)
    .setCorrection(TypicalLEDStrip)
    .setDither(BRIGHTNESS < 255);

  // set master brightness control
  FastLED.setBrightness(BRIGHTNESS);
}
void processPin(byte pin) {
uint8_t trigger = getPinChangeInterruptTrigger(digitalPinToPCINT(channel_pin[pin]));
if(trigger == RISING) {
    rising_start[pin] = micros();
  } else if(trigger == FALLING) {
    channel_length[pin] = micros() - rising_start[pin];
  }
}
void onRising0(void) {
processPin(0);
}
void onRising1(void) {
processPin(1);
}
void onRising2(void) {
processPin(2);
}
void loop() {
  if (channel_length[1]>1000){
      //pride();
      Serial.println(channel_length[1]);
      value = map (channel_length[0],972,2000,0,255);
      //Serial.println(value);
turnright();
  }
  if (channel_length[1]<1000){
      FastLED.setBrightness(0);
      FastLED.show();  
  }
  
  Serial.print(channel_length[0]);
  Serial.print(" | ");
  Serial.print(channel_length[1]);
  Serial.print(" | ");
  Serial.print(value);
  Serial.println("");
delay(100);
}

void pride() 
{
  static uint16_t sPseudotime = 0;
  static uint16_t sLastMillis = 0;
  static uint16_t sHue16 = 0;
 
  uint8_t sat8 = beatsin88( 87, 220, 250);
  uint8_t brightdepth = beatsin88( 341, 96, 224);
  uint16_t brightnessthetainc16 = beatsin88( 203, (25 * 256), (40 * 256));
  uint8_t msmultiplier = beatsin88(147, 23, 60);

  uint16_t hue16 = sHue16;//gHue * 256;
  uint16_t hueinc16 = beatsin88(113, 1, 3000);
  
  uint16_t ms = millis();
  uint16_t deltams = ms - sLastMillis ;
  sLastMillis  = ms;
  sPseudotime += deltams * msmultiplier;
  sHue16 += deltams * beatsin88( 400, 5,9);
  uint16_t brightnesstheta16 = sPseudotime;
  
  for( uint16_t i = 0 ; i < NUM_LEDS; i++) {
    hue16 += hueinc16;
    uint8_t hue8 = hue16 / 256;

    brightnesstheta16  += brightnessthetainc16;
    uint16_t b16 = sin16( brightnesstheta16  ) + 32768;

    uint16_t bri16 = (uint32_t)((uint32_t)b16 * (uint32_t)b16) / 65536;
    uint8_t bri8 = (uint32_t)(((uint32_t)bri16) * brightdepth) / 65536;
    bri8 += (255 - brightdepth);
    
    CRGB newcolor = CHSV( hue8, sat8, bri8);
    
    uint16_t pixelnumber = i;
    pixelnumber = (NUM_LEDS-1) - pixelnumber;
    
    nblend( leds[pixelnumber], newcolor, 64);
  }
}

void turnright(){
  //led 0 rechts
  //led 7 links
  leds[2] = CRGB::Yellow;
      FastLED.show(); 
  delay(20);
  leds[1] = CRGB::Yellow;
      FastLED.show(); 
   delay(20);
   leds[0] = CRGB::Yellow;
       FastLED.show(); 
   delay(20);
   leds[2] = CHSV(160,255,255);
       FastLED.show(); 
   delay(20);
   leds[2] = CHSV(0,255,255);
       FastLED.show(); 
   delay(20);
   leds[1] = CHSV(160,255,255);
       FastLED.show(); 
   delay(20);
   leds[1] = CHSV(0,255,255);
       FastLED.show(); 
   delay(20);
   leds[0] = CHSV(160,255,255);
       FastLED.show(); 
   delay(20);
   leds[0] = CHSV(0,255,255);
       FastLED.show(); 
   delay(20);


}
